import tkinter as tk
from tkinter import ttk, messagebox
import mysql.connector

class Database:
    def __init__(self):
        self.conn = mysql.connector.connect(
            host="localhost",
            user="root",
            password="keerthana@35",
            database="mydatabase"
        )
        self.cursor = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS userdata (
                register_number VARCHAR(255) PRIMARY KEY,
                username VARCHAR(255) UNIQUE NOT NULL,
                password VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL,
                date_of_birth DATE NOT NULL,
                branch VARCHAR(255) NOT NULL,
                cgpa FLOAT NOT NULL,
                backlogs INT NOT NULL,
                skills TEXT
            )
        """)
        self.conn.commit()

    def register_user(self, user_data):
        try:
            self.cursor.execute("""
                INSERT INTO userdata 
                (register_number, username, password, email, date_of_birth, branch, cgpa, backlogs, skills) 
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
            """, user_data)
            self.conn.commit()
            messagebox.showinfo("Registration Info", "Registration successful!")
        except mysql.connector.IntegrityError:
            messagebox.showerror("Registration Error", "Username or Register Number already exists.")

    def login(self, username, password):
        self.cursor.execute("SELECT * FROM userdata WHERE username=%s AND password=%s", (username, password))
        user = self.cursor.fetchone()
        if user:
            messagebox.showinfo("Login Successful", "Welcome!")
            return True
        else:
            messagebox.showerror("Login Failed", "Invalid username or password")
            return False

    def get_all_users(self):
        self.cursor.execute("SELECT username FROM userdata")
        users = self.cursor.fetchall()
        return [user[0] for user in users]

    def get_user_details(self, username):
        self.cursor.execute("SELECT * FROM userdata WHERE username=%s", (username,))
        user = self.cursor.fetchone()
        return user

class GUI:
    def __init__(self, root):
        self.root = root
        self.root.title("Login Page")
        self.root.geometry("800x500")

        self.database = Database()

        # Create a frame to hold login widgets
        login_frame = tk.Frame(self.root)
        login_frame.pack(expand=True, padx=200, pady=100)  # Adjust padx and pady as needed

        tk.Label(login_frame, text="Login", font=("Helvetica", 16, "bold")).pack(pady=10)

        self.username_var = tk.StringVar()
        self.password_var = tk.StringVar()

        tk.Label(login_frame, text="Username:").pack()
        tk.Entry(login_frame, textvariable=self.username_var).pack()

        tk.Label(login_frame, text="Password:").pack()
        tk.Entry(login_frame, textvariable=self.password_var, show='*').pack()

        tk.Button(login_frame, text="Login", command=self.login, bg='crimson', fg='white').pack(pady=10)

        tk.Button(login_frame, text="Sign Up", command=self.open_signup_page, bg='blue', fg='white').pack(pady=5)

    def login(self):
        username = self.username_var.get()
        password = self.password_var.get()
        if self.database.login(username, password):
            self.open_home_page()

    def open_signup_page(self):
        self.signup_window = tk.Toplevel(self.root)
        self.signup_window.title("Sign Up Page")
        window_width = 400
        window_height = 500
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        x_coordinate = (screen_width - window_width) / 2
        y_coordinate = (screen_height - window_height) / 2
        self.signup_window.geometry(f"{window_width}x{window_height}+{int(x_coordinate)}+{int(y_coordinate)}")

        signup_frame = tk.Frame(self.signup_window)
        signup_frame.pack(expand=True, padx=50, pady=50)

        self.new_register_number_var = tk.StringVar()
        self.new_username_var = tk.StringVar()
        self.new_password_var = tk.StringVar()
        self.confirm_password_var = tk.StringVar()
        self.email_var = tk.StringVar()
        self.date_of_birth_var = tk.StringVar()
        self.branch_var = tk.StringVar()
        self.cgpa_var = tk.DoubleVar()
        self.backlogs_var = tk.IntVar()
        self.skills_var = tk.StringVar()

        tk.Label(signup_frame, text="Register Number:").grid(row=0, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.new_register_number_var).grid(row=0, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Username:").grid(row=1, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.new_username_var).grid(row=1, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Password:").grid(row=2, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.new_password_var, show='*').grid(row=2, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Confirm Password:").grid(row=3, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.confirm_password_var, show='*').grid(row=3, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Email:").grid(row=4, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.email_var).grid(row=4, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Date of Birth (YYYY-MM-DD):").grid(row=5, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.date_of_birth_var).grid(row=5, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Branch:").grid(row=6, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.branch_var).grid(row=6, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="CGPA:").grid(row=7, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.cgpa_var).grid(row=7, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Backlogs:").grid(row=8, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.backlogs_var).grid(row=8, column=1, padx=10, pady=5)

        tk.Label(signup_frame, text="Skills:").grid(row=9, column=0, sticky="w", padx=10, pady=5)
        tk.Entry(signup_frame, textvariable=self.skills_var).grid(row=9, column=1, padx=10, pady=5)

        tk.Button(signup_frame, text="Register", command=self.register, bg='crimson', fg='white').grid(row=10, columnspan=2, pady=10)

        # Center the window on the screen
        self.signup_window.update_idletasks()
        self.signup_window.lift()

    def register(self):
        register_number = self.new_register_number_var.get()
        username = self.new_username_var.get()
        password = self.new_password_var.get()
        confirm_password = self.confirm_password_var.get()
        email = self.email_var.get()
        date_of_birth = self.date_of_birth_var.get()
        branch = self.branch_var.get()
        cgpa = self.cgpa_var.get()
        backlogs = self.backlogs_var.get()
        skills = self.skills_var.get()

        if password != confirm_password:
            messagebox.showerror("Password Mismatch", "Passwords do not match.")
            return

        user_data = (register_number, username, password, email, date_of_birth, branch, cgpa, backlogs, skills)
        self.database.register_user(user_data)

    def open_home_page(self):
        self.root.withdraw()  # Hide login page
        home_window = tk.Toplevel()
        home_window.title("Home Page")
        home_window.geometry("900x500")

        # Create a treeview widget to display members
        self.tree = ttk.Treeview(home_window, columns=("Register Number", "Username"), show="headings")
        self.tree.heading("Register Number", text="Register Number")
        self.tree.heading("Username", text="Username")
        self.tree.pack(expand=True, fill=tk.BOTH)

        users = self.database.get_all_users()
        for user in users:
            self.tree.insert("", "end", values=(user,))

        # Bind double-click event to show user details
        self.tree.bind("<Double-1>", self.show_user_details)

    def show_user_details(self, event):
        selected_item = self.tree.selection()[0]
        username = self.tree.item(selected_item, "values")[0]
        user_details = self.database.get_user_details(username)
        if user_details:
            user_details_window = tk.Toplevel()
            user_details_window.title("User Details")
            user_details_window.geometry("900x500")

            user_details_frame = tk.Frame(user_details_window)
            user_details_frame.pack(expand=True, padx=50, pady=50)

            tk.Label(user_details_frame, text="User Details", font=("Helvetica", 16, "bold")).pack(anchor="w", padx=10, pady=10)

            tk.Label(user_details_frame, text=f"Register Number: {user_details[0]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Username: {user_details[1]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Email: {user_details[3]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Date of Birth: {user_details[4]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Branch: {user_details[5]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"CGPA: {user_details[6]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Backlogs: {user_details[7]}").pack(anchor="w", padx=10, pady=2)
            tk.Label(user_details_frame, text=f"Skills: {user_details[8]}").pack(anchor="w", padx=10, pady=2)

if __name__ == "__main__":
    root = tk.Tk()
    app = GUI(root)
    root.mainloop()




       
