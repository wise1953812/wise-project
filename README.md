# College Placement System 

This is a simple project implemented using Python tkinter and MySQL 

## Description

This project is a web-based application that allows users to log in and sign up into the website ..
Our college placement system enables users to register into the website.
There is a login page in the first page ,if you have any login username you can easily into.If you are the new to the website then you must add click on the signup provided on the first page and then navigates to another page called signup page.There you can register your details like Username, Register Number,Date of Birth,Email,Branch,CGPA, Backlogs,Skills .It includes your data in it and stores in a separate page..After login into your page you get a welcome message then after pressing the ok button page will be open with your name .Next by clicking the Name our details will be shown as filled in register page..


## Technologies Used

- Python
- Tkinter
- Mysql

## Installation

1. Clone the repository:
git clone https://gitlab.com/wise1953812

2. Run the application:
python day5.py

## Usage
1.Run the application by executing demo.py
2.Log in with your credentials if not signup and then login
3.Then comes a usernames.so that the user can select from the given list
4.From their selection of usernames a list of details will be displayed